# 提示：重复使用组件

通常，您可能希望在 HTML 页面的多个位置显示 React 组件。 下面是一个例子，它显示了三次 “Like” 按钮 并将一些数据传递给它：


> **注意**  
> 这种策略通常非常有用，而且页面的 React 展示部分是相互隔离的。 在 React 代码中，使用component composition（组合组件） 会更容易。




参考：
- [将 React 添加到网站](http://react.html.cn/docs/add-react-to-a-website.html)
