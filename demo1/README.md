# 将 React 添加到网站


## 第1步：添加一个 DOM 容器到 HTML

首先，打开您想要编辑的 HTML 页面。 添加一个空的 <div> 标签来标记要用 React 显示内容的位置。 例如：

```
<!-- ... existing HTML ... -->

<div id="like_button_container"></div>

<!-- ... existing HTML ... -->
```

我们给这个 <div> 加一个唯一的 HTML 属性 id 。 这将允许我们稍后从 JavaScript 代码中找到它，并在其中显示一个 React 组件。

> ### Tip
> 您可以在 `<body>` 标签内的 任意 位置放置一个 `<div>` “容器”。根据需要，您可以在一个页面上拥有多个独立的 DOM 容器。 他们通常是空标签 — React 将替换 DOM 容器内的任何现有内容。


## 第2步： 添加 Script 标签

接下来，在闭合的 `</body>` 标签之前，向 HTML 页面添加三个 `<script>` 标签：

```
<!-- ... other HTML ... -->

  <!-- 加载 React. -->
  <!-- 注意：部署时，将"development.js"替换为"production.min.js"。 -->
  <script src="https://unpkg.com/react@16/umd/react.development.js" crossorigin></script>
  <script src="https://unpkg.com/react-dom@16/umd/react-dom.development.js" crossorigin></script>

  <!-- 加载我们 React 组件。 -->
  <script src="like_button.js"></script>

</body>
```

前两个标签加载 React 。 第三个将加载你的组件代码。



## 第3步：创建一个 React 组件

在 HTML 页面文件的同级目录下创建一个名为 like_button.js 的文件,并添加代码。


OK.到此打开index.html可以看到按钮已经被加载到页面中。


参考：
- [将 React 添加到网站](http://react.html.cn/docs/add-react-to-a-website.html)