# 尝试通过 JSX 使用 React

在上面的例子中，我们只依赖浏览器原生支持的功能。 下面的代码是我们使用 JavaScript 函数调用来告诉 React 要显示的内容：

```
const e = React.createElement;

// Display a "Like" <button>
return e(
  'button',
  { onClick: () => this.setState({ liked: true }) },
  'Like'
);
```

但是，React 还提供了一个使用 JSX 的选项：

```
// Display a "Like" <button>
return (
  <button onClick={() => this.setState({ liked: true })}>
    Like
  </button>
);
```

这两个代码片段是等同的。 虽然 JSX 是 完全可选， 但是许多人认为它有助于编写UI代码 - 无论是使用 React 还是其他库。


## 将 JSX 添加到项目

将 JSX 添加到项目中并不需要打包器或开发服务器等复杂的工具。 从本质上讲，添加 JSX 就像添加 CSS 预处理器一样。 唯一的要求是在您的计算机上安装 Node.js。

跳转到终端中的项目文件夹，然后粘贴这两个命令：

1. 步骤1： 运行 npm init -y （如果失败，这是修复办法）

2. 步骤2： 运行 npm install babel-cli@6 babel-preset-react-app@3

> **提示**  
我们 在这里只使用 npm 来安装 JSX 预处理器; 你不会再需要它。React 和应用程序代码都可以保留为 `<script>` 标签而不做任何更改。

恭喜！ 您只需将 生产就绪 JSX 安装程序 添加到您的项目中即可。


# 运行JSX预处理器

创建一个名为 src 的文件夹并运行这个终端命令：

```
npx babel --watch src --out-dir . --presets react-app/prod 
```

> **注意：**  
> npx 不是拼写错误 — 它是 npm 5.2+ 附带的包运行工具。 
> 
> 如果您看到错误消息：“You have mistakenly installed the babel package”，你可能错过了 上一步 。在同一个文件夹中执行它，然后重试。

不需要等待它完成 - 此命令是启动 JSX 的自动观察器。

如果你现在用这个 JSX入门代码 创建一个src/like_button.js 文件， 观察器将使用适合浏览器的普通 JavaScript 代码创建一个预处理的 like_button.js 。 当您使用 JSX 编辑源文件时， 该转换将自动重新运行。







参考：
- [将 React 添加到网站](http://react.html.cn/docs/add-react-to-a-website.html)